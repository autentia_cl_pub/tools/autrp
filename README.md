# Autentia Reverse Proxy

Autentia Reverse Proxy is a shell script to compile and install a non-ssl reverse proxy to [https://www.autentia.cl](https://www.autentia.cl "https://www.autentia.cl") using [NGINX](https://nginx.org "NGINX")

# Getting Started
__Disclaimer:__ _This installer is only tested on Debian 9 (Stretch), but maybe work on any Debian based OS or other GNU/Linux like OS_

### Prerequisites
You need this packages before run the script:
  - lsb-release
  - wget
  - gcc
  - libpcre3-dev
  - zlib1g-dev
  - make
  - libssl-dev

### Basic Installation
You can create a file with the content of _install.sh_ script, give execution permissions and run with _./install.sh_

#### via curl

```shell
bash -c "$(curl -fsSL https://gitlab.com/autentia_cl_pub/tools/autrp/raw/master/install.sh)"
```
#### via wget

```shell
bash -c "$(wget -O- https://gitlab.com/autentia_cl_pub/tools/autrp/raw/master/install.sh)"
```