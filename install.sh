#!/bin/bash
# lsb-release wget gcc libpcre3-dev zlib1g-dev make libssl-dev
# contrib non-free

set -e

LSB=$(lsb_release -si)
INSTALLDIR="/opt/nginx"
BINDIR="/usr/bin"
CONFPATH="/opt/nginx/conf"
LOGPATH="/var/log/nginx"
PIDPATH="/var/run/nginx"
LOCKPATH="/run"
OPTIONS="--with-http_ssl_module --prefix=$INSTALLDIR --sbin-path=$BINDIR --pid-path=$PIDPATH/nginx.pid --lock-path=$LOCKPATH/nginx.lock --conf-path=$CONFPATH/nginx.conf --error-log-path=$LOGPATH/error.log --http-log-path=$LOGPATH/access.log"
NCONF=$(cat <<-EOF
worker_processes  1;

events {
  worker_connections  1024;
}

http {
  include           mime.types;
  default_type      application/octet-stream;
  sendfile          on;
  keepalive_timeout 65;

  server {
    listen 80;
    listen [::]:80;

    access_log /var/log/nginx/reverse-access.log;
    error_log /var/log/nginx/reverse-error.log;

    location / {
      proxy_ssl_server_name on;
      proxy_pass https://www.autentia.cl:443/;
    }
  }
}
EOF
)

get-version(){
  echo "Geting last NGINX version..."
  LAST=$(wget -qO- https://nginx.org/en/CHANGES |head -n2|tail -n1)
  IFS=' '
  read -ra VER <<< "$LAST"
  VERSION="${VER[3]}"
  if [ ! $VERSION ]; then
    echo "Impossible to get NGINX version... exiting..."
    exit
  else
    echo "Done! the version is: $VERSION"
  fi
}

prepare(){
  echo "Preparing..."
  LPCRE=$(find /usr/ -type f -iname '*libpcre*' |wc -l)
  LSSL=$(find /usr/ -type f -iname '*libssl.a*' |wc -l)
  LZLIB=$(find /usr/ -type f -iname '*zlib.h*' |wc -l)
  if [ ! /usr/bin/lsb_release ]; then
    echo "Please install lsb_release, binary not found"
    exit
  elif [ ! /usr/bin/wget ]; then
    echo "Please install wget, binary not found"
    exit
  elif [ ! /usr/bin/gcc ]; then
    echo "Please install gcc, binary not found"
    exit
  elif [ ! /usr/bin/make ]; then
    echo "Please install make, binary not found"
    exit
  elif [ $LPCRE == 0 ]; then
    echo "Please install libpcre3–dev, library not found"
    exit
  elif [ $LSSL == 0 ]; then
    echo "Please install libssl-dev, library not found"
    exit
  else
    echo "All dependencies are OK!"
  fi
}

download(){
  echo "Downloading..."
  if [ ! -f /usr/src/nginx-$VERSION.tar.gz ]; then
    wget -c https://nginx.org/download/nginx-$VERSION.tar.gz -O /usr/src/nginx-$VERSION.tar.gz
  else
    echo "Version already downloaded, please remove."
    exit
  fi
}

work(){
  echo "Compiling..."
  cd /usr/src/
  tar zxf nginx-$VERSION.tar.gz
  cd nginx-$VERSION
  ./configure $OPTIONS
  make
  make install
  echo $NCONF > $CONFPATH/nginx.conf
  nginx
}

prepare
get-version
download
work